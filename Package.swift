// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "Identomat",
    defaultLocalization: "en",
    platforms: [.iOS(.v10)],
    products: [
        .library(
            name: "Identomat",
            targets: ["identomat"]),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(
            name: "identomat",
            path: "identomat.xcframework"
        )
    ]
)
